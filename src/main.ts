import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Antd from "ant-design-vue";
// import "ant-design-vue/dist/antd.css";
import { createI18n } from "vue-i18n";
import messages from "./messages";

export const i18n = createI18n({
  locale: "fa",
  fallbackLocale: "en",
  messages,
});

createApp(App).use(store).use(router).use(Antd).use(i18n).mount("#app");
