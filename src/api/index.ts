import axios from "axios";

export const getWeather = async (lat = "", lng = "") => {
  const result = axios.get(
    `https://api.open-meteo.com/v1/forecast?latitude=${lat}&longitude=${lng}&current_weather=true`
  );
  return result;
};
