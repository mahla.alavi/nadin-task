import { createStore } from "vuex";
const themeData = JSON.parse(localStorage.getItem("theme") ?? "[]") || "";
const languageData = JSON.parse(localStorage.getItem("language") ?? "[]") || "";
const userNameData = JSON.parse(localStorage.getItem("userName") ?? "[]") || "";

export default createStore({
  state: {
    them: themeData,
    language: languageData,
    userName: userNameData,
  },
  getters: {},
  mutations: {
    setTheme(state, theme) {
      state.them = theme;
    },
    setUserName(state, name) {
      state.userName = name;
    },
    setLanguage(state, language) {
      state.language = language;
    },
  },
  actions: {},
  modules: {},
});
