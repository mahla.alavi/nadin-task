const messages = {
  en: {
    message: {
      hello: "hello world",
    },
    menu: {
      dashboard: "Dashboard",
      todos: "TODOs",
      weather: "Weather",
      profile: "Profile",
    },
    welcomeMessage: {
      goodMorning: "Good morning",
      goodAfternoon: "Good afternoon",
      goodEvening: "Good evening",
      goodNight: "Good night",
    },
    todoList: {
      todoList: "ToDo List",
      item: "Item",
      addToDo: "add ToDo",
      editToDo: "edit ToDo",
    },
    profile: {
      saveButton: "Save",
      theme: "Them",
      language: "Language",
    },
  },
  fa: {
    message: {
      hello: "سلام",
    },
    menu: {
      dashboard: "داشبورد",
      todos: "لیست وظایف",
      weather: "آب و هوا",
      profile: "پروفایل",
    },
    welcomeMessage: {
      goodMorning: "صبح بخیر",
      goodAfternoon: "ظهر بخیر",
      goodEvening: "عصر بخیر",
      goodNight: "شب بخیر",
    },
    todoList: {
      todoList: "لیست وظایف",
      item: "آیتم",
      addToDo: "اضافه",
      editToDo: "ویرایش",
    },
    profile: {
      saveButton: "ذخیره",
      theme: "زمینه",
      language: "زبان",
    },
  },
};

export default messages;
